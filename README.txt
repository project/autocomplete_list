CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module provides a widget for a multi-valued entity reference field.
It is represented as an autocomplete field with values organized as a list with ability to delete each item.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/autocomplete_list

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/autocomplete_list


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.


MAINTAINERS
-----------

Current maintainers:
 * Artem Kolotilkin (temkin) - https://www.drupal.org/u/temkin
 * Illia Stankevych (eliasbrat) - https://www.drupal.org/u/eliasbrat
